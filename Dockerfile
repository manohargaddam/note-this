FROM node:8

LABEL Author="manohar.gaddam@live.com"

# Copy app to /src
COPY . /src

# Install app and dependencies into /src
RUN cd /src; npm install --production

EXPOSE 80

CMD cd /src && node app.js --NODE_ENV production --PORT 80