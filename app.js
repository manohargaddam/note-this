var express = require('express'),
    bodyParser = require('body-parser'),
    app = express(),
    path = require('path');

//auth:begin
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');
const config = require('./config');
const oauth2 = require('./src/api/auth/oauth2');
const defaultHandlers = require('./src/api/auth/defaultHandlers');
const apiControllerRouter = require('./src/api/controllers/init');

// [START session]
// Configure the session and session storage.
const sessionConfig = {
    resave: false,
    saveUninitialized: false,
    secret: config.get('SECRET'),
    signed: true,
    store: new MongoStore({
        url: config.get('MONGO_URL')
    })
};

app.use(session(sessionConfig));
// [END session]

//health
app.get('/api/health', (req, res) =>res.send("All is well!"));

// OAuth2
app.use(passport.initialize());
app.use(passport.session());
app.use(oauth2.router);
app.use(express.static('./src/public'));
app.use(oauth2.required);
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(apiControllerRouter);
app.use(express.static('./src/views'));
app.use(defaultHandlers.basic404Handler);
app.use(defaultHandlers.basicErrorHandler);

const server = app.listen(config.get('PORT'), () => {
    const port = server.address().port;
    console.log(`App listening on port ${port}`);
});

require('./src/api/controllers/socketHandler')(server);
module.exports = app;