'use strict';

// Hierarchical node.js configuration with command-line arguments, environment
// variables, and files.
const nconf = module.exports = require('nconf');
const path = require('path');
const process = require('process');
const argv = require('yargs').argv;

const env = argv.NODE_ENV || process.env.NODE_ENV;
console.log(env);
var configFile = env !== 'develop' ? 'config-prod.json':'config-local.json';

nconf
    // 1. Command-line arguments
    .argv()
    // 2. Environment variables
    .env()  
    // 3. Config file
    .file("defaults",path.join(__dirname, 'config/config-default.json'))
    .file("env-specific",path.join(__dirname,'config/', configFile));

// Check for required settings
checkConfig('GCLOUD_PROJECT');
checkConfig('OAUTH2_CLIENT_ID');
checkConfig('OAUTH2_CLIENT_SECRET');

function checkConfig(setting) {
    if (!nconf.get(setting)) {
        throw new Error(`You must set ${setting} as an environment variable or in config.json!`);
    }
}