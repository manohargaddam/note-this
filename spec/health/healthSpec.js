var request = require('request'),
    app = require('../../app');

describe("Health, ", function() {
    it("api/health should return 200!", function(done) {
      request.get('http://localhost:3000/api/health', function (err, res, body){
        expect(res).toBeDefined();
        expect(res.statusCode).toBe(200);
        expect(res.body).toBe('All is well!');
        done();
      });
    });
});