var gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    jasmine = require('jasmine'),
    runTests = require('./spec/support/runJasmine');

gulp.task('default', function () {
        nodemon({
                script: 'app.js',
                ext: 'js',
                env: {
                    PORT: 3000
                },
                ignore: ['node_modules/**']
            })
            .on(function () {
                console.log('Restarting:)');
            });
    })
    .task('dev',['default','dev_env'])
    .task('prod',['default','prod_env'])
    .task('test',['dev_env'], runTests)
    .task('test_prod',['prod_env'], runTests)
    .task('dev_env', () => process.env.NODE_ENV = 'develop')
    .task('prod_env',() => process.env.NODE_ENV = 'production');