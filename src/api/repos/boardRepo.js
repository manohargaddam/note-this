var Board = require("../models/boardModel");
var userRepo = require("./userRepo");

var boardRepo = {};

boardRepo.getAllboards = function (userId, done) {
    userRepo.allBoardIds(userId, (err, boardIds) => {
        const boardFiler = {_id: {$in: boardIds}};
        const boardFields = '_id name';
        Board.find(boardFiler,boardFields,done);
    });
};

boardRepo.addNewBoard = function (userId, done) {
    var newBoard = new Board({name: 'Your new board.', cards:[{
        desc:'sample card',
        left:100, 
        top:200, 
        height:110
    }]});
    newBoard.save().then((board) => 
                        userRepo.addABoard(userId, board._id, 
                            () => done(false, board)));
};

boardRepo.getBoardById = function (boardId, done) {
    Board.findById(boardId, done);
};

boardRepo.updateBoardName = function (boardId, name, done) {
    var filter = {_id: boardId},
    update = {$set: {name: name}};
    Board.update(filter, update, done);
};

boardRepo.updateBoardWidth = function (boardId, width, done) {
    var filter = {_id: boardId},
    update = {$set: {width: width}};
    Board.update(filter, update, done);
};

boardRepo.updateBoardHeight = function (boardId, height, done) {
    var filter = {_id: boardId},
    update = {$set: {height: height}};
    Board.update(filter, update, done);
};

boardRepo.updateBoardAddUser = function (boardId, email, done) {
    var filter = {_id: boardId},
        update = {$addToSet : {access : email}};
    userRepo.giveAccessToBoard(email, boardId, (err) => {
        if (err) done(true);
        else Board.update(filter, update, () => done(false));
    });
};

boardRepo.deleteBoard = function (boardId, done) {
    Board.findByIdAndRemove(boardId, done);
};

module.exports = boardRepo;