var User = require("../models/userModel");
var _ = require('lodash');

var userRepo = {};

userRepo.giveAccessToBoard = function (email, boardId, done) {
    userRepo.userExists(email, (err, user) => {
        if (err) done(true);
        else if(!_.find(user.boards.toObject(), {id:boardId})){
            user.boards.push({id:boardId, access:'member'});
            user.save();
            done(false);
        }
        else done(false);
    });
};

userRepo.allBoardIds = function (userId, done) {
    const userFilter = {_id:userId};
    const userFields = 'boards';
    User.findById(userId, userFields, (err, user) => {
        var accessList = user.toObject().boards
        var boardIds = _.map(_.filter(accessList,{access: 'owner'}), (each) => each.id);
        done(false, boardIds);
    })
};

userRepo.userExists = function (email, done) {
    User.findOne({email:email}, (err, user) => {
        if (!user || err)
            done(true);
        else
            done(false, user);
    });
};

userRepo.addABoard = function (userId, boardId, done) {
    var addToUser = {$push : { "boards": {id: boardId, access: 'owner'}}};
    User.findByIdAndUpdate(userId, addToUser, done);
};

userRepo.addUser = function (profile, done) {
    User.findById(profile._id, function (err, user) {
        if(!user){
          var user = new User(profile);
          user.save();
        }
        done(null, profile);
    });
};

module.exports = userRepo;