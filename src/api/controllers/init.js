var express = require('express'),
    router = express.Router(),   
    mongoose = require('mongoose'),
    config = require('./../../../config');

const oauth2 = require('./../auth/oauth2');
    
var db = mongoose.connect(config.get('MONGO_URL'));
var bodyParser = require('body-parser');
router.use('/api/boards', require('./boardController'));
router.use('/api/cards', require('./cardController'));
router.use('/api/user', require('./userController'));
//middleware
module.exports = router;