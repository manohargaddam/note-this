var userRouter = require('express').Router();
var boardAuthorizer = require('./../auth/boardAuthorizer');

userRouter.route("/")
    .get(function (req, res) {
        res.json(req.user);
    })

module.exports = userRouter;