function spinUpSocket(server) {
    var io = require('socket.io').listen(server);

    var channels = [];

    io.on('connection', function (socket) {

        socket.on('client.board.join',function (data) {
            socket.join(data.boardId);
        });

        socket.on('client.board.update', function (data) {
            socket.to(data.boardId).emit('server.board.update', {boardId: data.boardId});
        });
        console.log('Client connected!');
    });
}

module.exports = spinUpSocket;