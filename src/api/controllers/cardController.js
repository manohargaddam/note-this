var express = require('express');
var cardRouter = express.Router();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var Board = require("../models/boardModel");
var boardAuthorizer = require('./../auth/boardAuthorizer');
var boardRepo = require('./../repos/boardRepo');

cardRouter.use("/:boardId", boardAuthorizer);
cardRouter.route("/:boardId")
.post(function (req, res) {
    var cond = {_id : req.params.boardId},
        update = {$push : {"cards":req.body}};
        
    Board.update(cond, update,function (err, doc) {
        err ? res.sendStatus(500): res.sendStatus(200);
    });
});

cardRouter.use("/:boardId/:cardId", boardAuthorizer);
cardRouter.route("/:boardId/:cardId")
.post(function (req, res) {
    var cond = {_id:req.params.boardId, "cards._id": req.params.cardId},
        update = { "$set" : {"cards.$": req.body}};
        
    Board.update(cond, update,function (err, doc) {
        err ? res.sendStatus(500): res.sendStatus(200);        
    });
})
.delete(function(req,res){
    var cond = {_id : req.params.boardId},
     update = {$pull : {cards :{_id: req.params.cardId}}};

    Board.update(cond, update,function (err, doc) {
        err ? res.sendStatus(500): res.sendStatus(200);        
    });
});

module.exports = cardRouter;