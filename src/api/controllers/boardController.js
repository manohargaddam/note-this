var boardRouter = require('express').Router();
var boardRepo = require('./../repos/boardRepo');
var boardAuthorizer = require('./../auth/boardAuthorizer');

boardRouter.route("/")
    .get(function (req, res) {
        boardRepo.getAllboards(req.user._id, 
            (err, boards) => err ? res.sendStatus(500) : res.json(boards));
    })
    .post(function (req, res) {
        var board = boardRepo.addNewBoard(req.user._id, 
            (err, board) => err ? res.sendStatus(500): res.json(board))
    });

boardRouter.use("/:boardId", boardAuthorizer);
boardRouter.route("/:boardId")
    .get(function (req, res) {
        boardRepo.getBoardById(req.params.boardId, 
            (err, board) => err ? res.sendStatus(500): res.json(board));
    })
    .post(function (req, res) {
        if (req.body.action == 'update-name')
            boardRepo.updateBoardName(req.params.boardId, req.body.name, 
                (err, board) => err ? res.sendStatus(500): res.json(board));
        else if (req.body.action == 'update-width')
            boardRepo.updateBoardWidth(req.params.boardId, req.body.width, 
                (err, board) => err ? res.sendStatus(500): res.json(board));
        else if (req.body.action == 'update-height')
            boardRepo.updateBoardHeight(req.params.boardId, req.body.height,
                (err, board) => err ? res.sendStatus(500): res.json(board));
        else if (req.body.action == 'add-user')
            boardRepo.updateBoardAddUser(req.params.boardId, req.body.email, 
                err => err ? res.sendStatus(500): res.sendStatus(200));
    })
    .delete(function (req, res) {
        boardRepo.deleteBoard(req.params.boardId, (err, board) => err ? res.sendStatus(500): res.json(board));
    });

module.exports = boardRouter;