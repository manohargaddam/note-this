module.exports = {
    basic404Handler : (req, res) => {
        res.status(404).send('Not Found');
    },
    basicErrorHandler : (err, req, res, next) => {
        res.status(500).send(err.response || 'Something broke!');
    }
}