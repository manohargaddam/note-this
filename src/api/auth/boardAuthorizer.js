var User = require("../models/userModel");
var mongoose = require('mongoose');
var _ = require('lodash');

module.exports = function (req, res, next) {
    User.findById(req.user._id,(err, user)=>{
        if(_.find(user.toObject().boards, {id : req.params.boardId}))
            next();
        else
            res.sendStatus(403);
    });  
};