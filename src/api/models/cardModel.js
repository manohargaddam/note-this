var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var cardModel = new Schema({
    _id: {type:String, default: guid()},
    desc :String,
    color :String,
    top :Number,
    left :Number,
    height :Number,
    createOn :{type:String, default: Date.now}
});

function guid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

module.exports = cardModel;