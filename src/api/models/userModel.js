var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var userModel = new Schema({
    _id: String,
    email: String,
    imageUrl: String,
    displayName: String,
    boards: [{id: String, access: String}],
    createOn :{type:String, default: Date.now}
});

module.exports = mongoose.model('User', userModel);