var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    cardModel = require('./cardModel');

var boardModel = new Schema({
    name: String,
    width: {type:String, default: "2000"},
    height: {type:String, default: "2000"},
    cards: [ cardModel ],
    access: [ String ],
    createOn :{type:String, default: Date.now}
});

module.exports = mongoose.model('Board', boardModel);