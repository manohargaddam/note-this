define([], function () {
    var dashboardService = function ($q, $http) {
        var service = {};

        service.getBoards = function () {
            var deffered = $q.defer();
            $http.get('api/boards').then(defaultResolver(deffered));
            return deffered.promise;
        };
        service.updateBoard = function (boardId, data) {
            var deffered = $q.defer();
            $http.post('api/boards/' + boardId, data).then(defaultResolver(deffered));
            return deffered.promise;
        };
        service.newBoard = function () {
            var deffered = $q.defer();
            $http.post('api/boards').then(defaultResolver(deffered));
            return deffered.promise;
        };
        service.deleteBoard = function (boardId) {
            var deffered = $q.defer();
            $http.delete('api/boards/' + boardId).then(defaultResolver(deffered));
            return deffered.promise;
        };

        var defaultResolver = function(deffered) {
            return function (resp) {
                deffered.resolve(resp.data);
            };
        };

        return service;
    }

    dashboardService.$inject = ['$q', '$http'];

    return dashboardService;
});