define([], function () {
    var dashboardController = function ($scope, dashboardService, ngToast) {
        $scope.boards =  [];

        $scope.updateBoardName = function (boardId, board) {
            var data = {action:'update-name', name : board.name};
            dashboardService.updateBoard(boardId, data).then(loadBoards);
            board.__mode = "saving";
        };
        

        $scope.updateBoardAddUser = function (boardId, board, email) {
            var data = {action:'add-user', email : email};
            dashboardService.updateBoard(boardId, data).then(function () {
                ngToast.create({
                    content: '<b>' + email + '</b> now has access!',
                    className : 'success',
                    timeout : 5000
                });
            });
        };

        $scope.newBoard = function () {
            dashboardService.newBoard().then(loadBoards);
        };
        $scope.deleteBoard = function (boardId) {
            dashboardService.deleteBoard(boardId).then(loadBoards);
        };

        $scope.switchToEditMode = function (board) {
            board.__mode = "edit";
            board.__name = board.name;
        };

        $scope.cancelName = function (board) {
            board.name = board.__name;
            board.__mode = null;
        };

        var loadBoards = function () {
            dashboardService.getBoards().then(function(boards){
                $scope.boards = boards;
            });
        };

        //init
        loadBoards();
    }

    dashboardController.$inject = ['$scope', 'dashboardService','ngToast'];

    return dashboardController;
});