define([], function () {
    var boardService = function ($q, $http, socket) {
        var service = {};

        service.getBoard = function (id) {
            var deffered = $q.defer();
            $http.get('api/boards/' + id).then(defaultResolver(deffered));
            return deffered.promise;
        };

        service.addCard = function (boardId, card) {
            var deffered = $q.defer();
            $http.post('api/cards/' + boardId, card).then(function (res) {
                deffered.resolve(res.data);
                socket.emit('client.board.update',{boardId:boardId});
            });
            return deffered.promise;
        };

        service.updateBoard = function (boardId, data) {
            var deffered = $q.defer();
            $http.post('api/boards/' + boardId, data).then(function (res) {
                deffered.resolve(res.data);
                socket.emit('client.board.update',{boardId:boardId});
            });
            return deffered.promise;
        };
        
        var defaultResolver = function(deffered) {
            return function (resp) {
                deffered.resolve(resp.data);
            };
        }

        return service;
    }

    boardService.$inject = ['$q', '$http', 'socket'];

    return boardService;
});
