define([], function () {
    var boardController = function ($scope, boardService, $stateParams, general, socket, hash) {
        $scope.colors = ['#ADD8E6', '#bd88eecf', '#f5a1a1', '#ffd8ab','#a8b4f7'];
        $scope.boardId = $stateParams.id;
        $scope.hashes = [];
        $scope.zoom = 900;
        var loadBoard = function () {
            boardService.getBoard($scope.boardId).then(loadPage);
        };

        var loadPage = function (board) {
            $scope.board = board;
            $scope.hashes = _.map(board.cards, function (card) {
                return {_id: card._id, hash: hash(card)};
            });
        }
        
        $scope.addNewCard = function ($event, boardId) {
            if($event.target !== $event.currentTarget)
                return;
            var newCard = {_id: guid(), left:$event.pageX, top:$event.pageY, height:110};
            $scope.board.cards.push(newCard);
            $scope.hashes.push({_id: newCard._id, hash: hash(newCard)});
            boardService.addCard(boardId, newCard);
        };

        $scope.updateBoardWidth = function (width) {
            var board = {_id : $scope.board._id, width : width, action : 'update-width'};
            boardService.updateBoard($scope.board._id, board);
        };

        $scope.updateBoardHeight = function (height) {
            var board = {_id : $scope.board._id, height : height, action : 'update-height'};
            boardService.updateBoard($scope.board._id, board);
        };

        $scope.zoomBoard = function (event) {
            console.log(event.target.value);
        };

        $scope.preventDrag = function (event) {
           event.stopPropagation();
        };

        function guid() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
              var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
              return v.toString(16);
            });
          }

        socket.emit('client.board.join', {boardId: $scope.boardId});
        socket.on('server.board.update', loadBoard);

        $scope.reload = loadBoard;

        loadBoard();
    };

    boardController.$inject = ['$scope', 'boardService', '$stateParams', 'generalService','socket','hash'];

    return boardController;
});