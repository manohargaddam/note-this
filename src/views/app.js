define(['angular',
    'uiRouter',
    'ngToast',
    'angularSanitize',
    'underscore',
    'dashboard/dashboardController',
    'dashboard/dashboardService',
    'board/boardController',
    'board/boardService',
    'utils/generalService',
    'card/cardDirective',
    'card/cardController',
    'card/cardService',
    'utils/socketService',
    'utils/hashService',
    'main/mainController',
    'main/userService'
  ],
  function (angular,
    uiRouter,
    ngToast,
    angularSanitize,
    underscore,
    dashboardController,
    dashboardService,
    boardController,
    boardService,
    generalService,
    cardDirective,
    cardController,
    cardService,
    socketService,
    hashService,
    mainController,
    userService) {

    var app = angular.module("note-this", ['ui.router','ngToast']);

    app.controller('dashboardController', dashboardController);
    app.service('dashboardService', dashboardService);
    app.controller('boardController', boardController);
    app.service('boardService', boardService);
    app.service('generalService', generalService);
    app.directive('card', cardDirective);
    app.controller('cardController', cardController);
    app.service('cardService', cardService);
    app.controller('mainController', mainController);
    app.service('userService', userService);
    app.service('socket', socketService);
    app.service('hash', hashService);

    app.config(['ngToastProvider', function(ngToast) {
      ngToast.configure({horizontalPosition: 'center'});
    }]);
    //routing
    app.config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise('/dashboard');

      $stateProvider
        .state('home', {
          url: '/home',
          controller:'homeController',
          templateUrl: 'home.html'
        })
        .state('dashboard', {
          url: '/dashboard',
          controller: 'dashboardController',
          templateUrl: '/dashboard/dashboard.html'
        })
        .state('board', {
          url: '/board/:id',
          controller: 'boardController',
          templateUrl: '/board/board.html'
        })
        .state('about', {
          url: '/about',
          templateUrl: 'about.html'
        })
    });

    app.init = function () {
      angular.bootstrap(document, ['note-this']);
    };

    return app;
  });