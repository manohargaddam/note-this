require.config({
    paths: {
        'angular': '/lib/angular.min',
        'underscore': '/lib/underscore.min',
        'ngToast': '/lib/ngToast.min',
        'angularSanitize': '/lib/angular-sanitize.min',
        'uiRouter': '/lib/angular-ui-router.min',
        'objectHash': '/lib/object-hash.min'
    },
    shim: {
        angular: {
            exports: 'angular'
        },
        underscore: {
            exports: 'underscore'
        },
        uiRouter: {
            exports: 'uiRouter'
        },
        angularSanitize: {
            deps: ['angular'],            
            exports: 'angularSanitize'
        },
        ngToast:{
            deps: ['angular'],
            exports: 'ngToast'            
        }
    }
});

require(['app'], function (app) {
    app.init();
});