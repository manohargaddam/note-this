define([], function () {
    var cardService = function ($q, $http, socket) {
        var service = {};
        
        service.deleteCard = function (boardId, cardId) {
            var deffered = $q.defer();
            $http.delete('api/cards/' + boardId + "/" + cardId).then(defaultResolver(deffered, boardId));
            return deffered.promise;
        };

        service.updateCard = function (boardId, cardId, card) {
            var deffered = $q.defer();
            $http.post('api/cards/' + boardId + '/' + cardId, card).then(defaultResolver(deffered, boardId));
            return deffered.promise;
        };
        
        var defaultResolver = function(deffered, boardId) {
            return function (resp) {
                deffered.resolve(resp.data);
                socket.emit('client.board.update',{boardId:boardId});                
            };
        };

        return service;
    }

    cardService.$inject = ['$q', '$http', 'socket'];

    return cardService;
});
