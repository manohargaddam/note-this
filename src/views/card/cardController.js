define([], function() {
    'use strict';
    var cardController = function ($scope, cardService, hash, $timeout) {
        $scope.deleteCard = function (boardId, cardId) {
            var card = _.findWhere($scope.board.cards, {"_id": cardId});
            var cardIndex = _.indexOf($scope.board.cards, card);
            $scope.board.cards.splice(cardIndex, 1);
            cardService.deleteCard(boardId, cardId);
        };

        $scope.changeColor = function (boardId, card, color) {
            card.color = color;
            $scope.updateCard();
        };

        $scope.dragDone = function ($card) {
            var offset = $card.offset();
            
            $scope.card.top = offset.top;
            $scope.card.left = offset.left;
            
            $scope.updateCard();
        };

        $scope.onresize = function (event) {
            var c_dim = event.target.getBoundingClientRect();
            if($scope.card.height !== c_dim.height){
                $scope.card.height = c_dim.height;
                $scope.updateCard();
            }
        };

        $scope.onTextChange = function () {
            var oldHash = _.findWhere($scope.hashes, {_id: $scope.card._id}).hash;
            (oldHash != hash($scope.card)) && $scope.updateCard();
        };

        $scope.whentyping = function () {
            if (!$scope.keydownSaveInprogess){
                $scope.keydownSaveInprogess = true;   
                $timeout(function () {
                    $scope.updateCard();
                    $scope.keydownSaveInprogess = false;
                },3000);
            }
        };

        $scope.resizeIfOverflow = function (event) {
            if(event.target.scrollHeight <= event.target.clientHeight) return;

            event.target.style.height = (event.target.scrollHeight + 5) + 'px';
            $scope.card.height = event.target.scrollHeight + 5;
            $scope.updateCard();
        };

        $scope.updateCard = function () {
            _.findWhere($scope.hashes, {_id: $scope.card._id}).hash = hash($scope.card);
            cardService.updateCard($scope.board._id, $scope.card._id, $scope.card);
        };
    };

    cardController.$inject = ['$scope','cardService', 'hash', '$timeout'];

    return cardController;
});