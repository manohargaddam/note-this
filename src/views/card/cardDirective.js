define([], function () {
  'use strict';
  var dir = function () {

    function link(scope, element, attrs) {
      scope.displayHeader = 'hidden';
      var board = $('.board');      
      $(element[0].firstChild).pep({
        elementsWithInteraction: 'textarea,nav',
        deferPlacement: true,
        removeMargins: false,
        useCSSTranslation: false,
        useBoundingClientRect:true,
        start: function (ev, obj) {
          obj.noCenter = false;
          obj.card = ev.target.parentElement.children[2];
        },
        drag: function (ev, obj) {
          var vel = obj.velocity();
          var rot = (vel.x) / 38;
          rotate(obj.$el, rot);
          if((ev.pageX + 500) > board.width()){
            board.width(board.width() + 1000);
            scope.updateBoardWidth(board.width());
          }
          if((ev.pageY + scope.card.height + 200) > board.height()){
            board.height(board.height() + 500);
            scope.updateBoardHeight(board.height());
          }
        },
        stop: function (ev, obj) {
          handleCentering(ev, obj);
          rotate(obj.$el, 0)
        },
        rest:function (ev, obj) {
          handleCentering(ev, obj);
          scope.$apply(function () {
            scope.dragDone($(obj.card));
          });
        }
      });

      function handleCentering(ev, obj) {
        if (obj.activeDropRegions.length > 0) {
          centerWithin(obj);
        }
      }

      function centerWithin(obj) {
        var $parent = obj.activeDropRegions[0];
        var pTop = $parent.position().top;
        var pLeft = $parent.position().left;
        var pHeight = $parent.outerHeight();
        var pWidth = $parent.outerWidth();

        var oTop = obj.$el.position().top;
        var oLeft = obj.$el.position().left;
        var oHeight = obj.$el.outerHeight();
        var oWidth = obj.$el.outerWidth();

        var cTop = pTop + (pHeight / 2);
        var cLeft = pLeft + (pWidth / 2);

        if (!obj.noCenter) {
          if (!obj.shouldUseCSSTranslation()) {
            var moveTop = cTop - (oHeight / 2);
            var moveLeft = cLeft - (oWidth / 2);
            obj.$el.animate({
              top: moveTop,
              left: moveLeft
            }, 50);
          } else {
            var moveTop = (cTop - oTop) - oHeight / 2;
            var moveLeft = (cLeft - oLeft) - oWidth / 2;

            obj.moveToUsingTransforms(moveTop, moveLeft);
          }

          obj.noCenter = true;
          return;
        }

        obj.noCenter = false;
      }

      function rotate($obj, deg) {
        $obj.css({
          "-webkit-transform": "rotate(" + deg + "deg)",
          "-moz-transform": "rotate(" + deg + "deg)",
          "-ms-transform": "rotate(" + deg + "deg)",
          "-o-transform": "rotate(" + deg + "deg)",
          "transform": "rotate(" + deg + "deg)"
        });
      }
    }

    return {
      restrict: 'E',
      scope: true,
      link: link,
      controller: 'cardController',
      templateUrl: '/card/card.html'
    };
  }

  return dir
});