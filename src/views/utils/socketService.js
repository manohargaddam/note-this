define([], function () {
    'use strict';

    var socket = function ($rootScope) {
        var service = {};
        var socket = io.connect();
        
        service.on = function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        }

        service.emit = function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        };

        return service;
    }

    socket.$inject = ['$rootScope']

    return socket;
});