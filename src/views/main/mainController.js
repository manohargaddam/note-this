define([], function () {
    var mainController = function ($scope, userService) {
        var loadUser = function () {
            userService.getUser().then(function (data) {
               $scope.user = data; 
            });
        };

        loadUser();
    };

    mainController.$inject = ['$scope', 'userService'];

    return mainController;
});