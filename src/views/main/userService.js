define([], function () {
    var userService = function ($q, $http) {
        var service = {};

        service.getUser = function () {
            var deffered = $q.defer();
            $http.get('api/user').then(function (res) {
                deffered.resolve(res.data);
            });
            return deffered.promise;
        };

        return service;
    }

    userService.$inject = ['$q', '$http'];

    return userService;
});
